# Dokkaebi

**[Logo Pendiente]**

* Dokkaebi es un sistema de administración para negocios de reparación de dispositivos electrónicos. El sistema pretende ofrecer transparencia al usuario sobre los procesos que se llevan a cabo con sus equipos electrónicos al momento de llevarlos a reparación. 

* Similar a los servicios de paquetería como Amazon, Dokkaebi mantendrá al cliente informado sobre el estado de reparación de su equipo cubriendo hasta los mas mínimos detalles, y en caso de solicitar envío a domicilio, el estado del envío. 

* Los objetivos principales de Dokkaebi es facilitar la gestión de las reparaciones a los empleados de empresas de reparación, y brindar confianza, transparencia y seguridad sobre el servicio a los clientes, asegurando que su dispositivo se encuentra en buenas manos y obteniendo el debido servicio.

## ¿Por qué?

Hoy en día los dispositivos electronicos son algo muy personal para todos, y dejar nuestro celular o laptop personal con un técnico desconocido no es algo que muchos hacen con mucha seguridad. Sin embargo, cuando un dispositivo falla, a veces son la única opción. 

Mientras que hay casos de una relación cliente-proveedor honesta, existen otros casos donde una de las partes se intenta aprovechar de la otra: Un técnico en reparación puede desear obtener mas ganancias aumentando los precios de sus servicios arbitrariamente o, aprovechando el usual desconocimiento técnico de los clientes, incluye necesidades que no van de acuerdo a lo que el cliente requiere, o por otra parte, el cliente puede realizar acusaciones falsas de negligencia en la reparación u otro tipo de daños.

Para evitar estas situaciones, Dokkaebi ofrece dar a ambas partes (cliente-técnico) una total transparencia sobre los procesos de reparación que se llevan a cabo sobre los dispositivos, manteniendo un registro a detalle sobre las reparaciones y los avances que se van realizando en las mismas.

## Funciones

- Rastreo de progreso de reparación:
  - Fecha y hora de ingreso de dispositivo al taller
  - Registro de Fecha y Hora de avances en reparación
  - Detalles de cada fase de reparación
    - Partes reemplazadas (Hardware)
    - Procesos realizados sobre Software/Sistema Operativo (Software)
  - Notificaciones a cliente
- Rastreo de Envío a domicilio de dispositivo
- Gestión de Dispositivos en taller
- Asignación de Dispositivos a Técnicos
- Envío de notificaciones a clientes

## Lenguajes implementados

- Django 2.2+
- Bootstrap 4
- VueJS 2

## Colaboración

- Proximamente