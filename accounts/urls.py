from django.urls import path, include
from . import views
from django.contrib.auth.models import User

app_name = 'accounts'

urlpatterns = [
    #path('', views.signup_view, name="account-main"),
    path('<str:username>', views.account, name="account"),
    path('login/', views.login_view, name="login"),
    path('client_signup/', views.client_signup, name="signup"),
    path('staff_signup/', views.staff_signup, name="staff_signup"),
    path('logout/', views.logout_view, name="logout"),
    path('update/', views.account_update, name="account_update")

]
