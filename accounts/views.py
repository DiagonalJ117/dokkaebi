from django.http import HttpResponse
from django.shortcuts import render, redirect, render_to_response
from django.contrib.auth.decorators import login_required, permission_required, user_passes_test
from django.views.decorators.csrf import csrf_exempt
from django.contrib import messages
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from .forms import Signup_UserCreationForm, UserProfileForm
from django.contrib.auth import login, logout
from django.contrib.auth.models import Permission, Group
from accounts.models import Usuario, Employee, Client, Address
from django.template import RequestContext
from dokkaebirepair.decorators import client_required, employee_required, permissions_required
#from django.views.decorators.csrf import csrf_exempt
# Create your views here.
#@csrf_exempt

@login_required(login_url='accounts:login')
@permission_required('can_add_usuario')
#signup para tecnicos/empleados
def staff_signup(request):
    role_title = 'Employee'
    if request.method=='POST':
        form = Signup_UserCreationForm(request.POST)
        if form.is_valid():
            #permission = Permission.objects.get(name='is_staff')
            
            user = form.save(commit=False)
            user.role = 2

            #automatically adding perms
            
            user.save()
            if user.role == 2:
                Employee.objects.create(profile=user, id_employee = user.user_id)
                group = Group.objects.get(name='employees')
                user.groups.add(group)
            #user.set_permissions.add(permission)
            #login(request, user)
            messages.success(request, 'Cuenta creada!')
            return redirect('home') #HttpResponse('Success!')

    else:
        
        form = Signup_UserCreationForm()

    return render(request, 'accounts/signup.html', {'form': form, 'role_title':role_title})

def client_signup(request):
    role_title = 'Client'
    if request.method == 'POST':
        form = Signup_UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.role == 1
            user.save()
            if user.role == 1:
                Client.objects.create(profile=user, id_client = user.user_id)
                group = Group.objects.get(name='clients')
                user.groups.add(group)
            login(request, user)
            messages.success(request, 'Cuenta de cliente creada!')
            return redirect('home')
    else:
        form = Signup_UserCreationForm()
    
    return render(request, 'accounts/signup.html', {'form': form, 'role_title':role_title})

#@permission_required('accounts.is_collab', raise_exception=True)
#def staffsignup_view(request):
#    if request.method=='POST':
#        form = Signup_UserCreationForm(request.POST)
#        if form.is_valid():
#            permission = Permission.objects.get(name='is_collab')
#            user = form.save()
#            user.set_permssions.add(permission)
#            login(request, user)
#            messages.success(request, 'Account created!')
#            return redirect('accounts:staff_signup') #HttpResponse('Success!')
#    else:
#        form = Signup_UserCreationForm()

#    return render(request, 'accounts/staffsignup.html', {'form': form})

def login_view(request):
    if request.method=='POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            #log in User
            user = form.get_user()
            if user.is_active == True:
                login(request, user)
            else:
                messages.error('Usuario bloqueado. Contacte a soporte técnico.')
            if 'next' in request.POST:
                return redirect(request.POST.get('next'))
            else:
                return redirect('accounts:account', request.user.username)
        else:
                messages.error(request, "Nombre de Usuario o contraseña incorrecta. NOTA: Nombre de Usuario debe ser en MINUSCULAS")
    else:
        form = AuthenticationForm()

    return render(request, 'accounts/login.html', {'form': form})

@login_required(login_url='accounts:login')
@permission_required('can_view_usuario')
def account(request, username):
    user = Usuario.objects.get(username=username)
    form = UserProfileForm(instance=user)
    return render(request,'accounts/account.html', {'user': user})


@login_required(login_url='accounts:login')
@permission_required('can_change_usuario')
def account_update(request, template_name="accounts/account_form.html"):
    if request.method=="POST":
        form = UserProfileForm(data=request.POST, instance=request.user)
        update = form.save(commit=False)
        update.user = request.user.username
        update.save()
        return redirect('accounts:account', request.user)
    else:
        form = UserProfileForm(instance=request.user)

    return render(request, 'accounts/account_form.html', {'form': form})


def logout_view(request):
    if request.method == 'POST':
        logout(request)
        return redirect('home')
    else:
        return HttpResponse('Not a POST request')
