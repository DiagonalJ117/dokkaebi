from django.contrib.auth.forms import UserCreationForm
from django import forms
from django.core.exceptions import ValidationError
from accounts.models import Usuario


class Signup_UserCreationForm(UserCreationForm):

    password1 = forms.CharField(label='Contraseña', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Confirmar Contraseña', widget=forms.PasswordInput)

    def clean_username(self):
        username = self.cleaned_data['username'].lower()
        r = Usuario.objects.filter(username=username)
        if r.count():
            raise ValidationError("Nombre de usuario no disponible.")
        return username
    
    class Meta:
        model = Usuario
        fields = ['username', 'email', 'first_name', 'last_name', 'password1', 'password2']
        widgets = {

        }

    def clean_email(self):
        email = self.cleaned_data['email'].lower()
        r = Usuario.objects.filter(email=email)
        if r.count():
            raise ValidationError("Cuenta con este correo ya existente.")
        return email

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')

        if password1 and password2 and password1 != password2:
            raise ValidationError("Contraseñas no coinciden.")

        return password1

    def save(self, commit=True):
        user = super(Signup_UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if user.first_name: 
            user.first_name = self.cleaned_data.get('first_name')
        if user.last_name:
            user.last_name = self.cleaned_data.get('last_name')
        if user.email :
            user.email = self.cleaned_data.get('email')
        if commit:
            user.save()
        return user

        

class UserProfileForm(forms.ModelForm):
    class Meta:
        model = Usuario
        fields = ['username', 'email', 'first_name', 'last_name']
        labels = {
            'username': 'Nombre de Usuario',
            'email': 'Correo Electronico',
            'first_name': 'Nombre',
            'last_name': 'Apellido'
        }


#class CollaboratorProfileForm(forms.ModelForm):
