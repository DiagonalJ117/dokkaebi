import os
import uuid
from django.utils.translation import ugettext as _
from django.core.validators import MinValueValidator
from django.db import models
from django.utils import timezone
from accounts.models import Client
from django.contrib.auth.models import User
from django import forms


class Device(models.Model):

    
    DEVICE_TYPE_CHOICES=(
        ('LAP', 'Laptop'),
        ('DESK', 'Desktop'),
        ('PHONE', 'Cellphone/Smartphone'),
        ('TABLET', 'Tablet'),
        ('AIO', 'All-In-One'),
    )

    DEV_POWER_CHOICES=(
        ('ON', 'Encendido'),
        ('OFF', 'Apagado') 
    )


    DEV_ASSEMBLY_CHOICES=(
        ('ARM', 'Armado'),
        ('DES', 'Desarmado')
    )


    DEV_CHARGECABLE_CHOICES=(
        ('CON', 'Con Cable de Carga'),
        ('SIN', 'Sin Cable de Carga'),
    )

    DEV_CHARGEADAPTER_CHOICES=(
        ('CON', 'Con Adaptador de Carga'),
        ('SIN', 'Sin Adaptador de Carga'),
    )


    DEV_BATTERY_CHOICES=(
        ('CON', 'Con Bateria'),
        ('SIN', 'Sin Bateria')
    )

    DEV_SIM_CHOICES=(
        ('CON', 'Con Tarjeta SIM'),
        ('SIN', 'Sin Tarjeta SIM')
    )

    DEV_CASE_CHOICES=(
        ('CON', 'Con Funda'),
        ('SIN', 'Sin Funda')
    )

    DEV_SDCARD_CHOICES=(
        ('CON', 'Con Memoria'),
        ('SIN', 'Sin Memoria')
    )
    id_device = models.AutoField(primary_key=True)
    client = models.ForeignKey(Client, default=None, on_delete=models.CASCADE)
    serialNumber = models.CharField(blank=True, max_length=40)
    devType = models.CharField(max_length=15, blank=True, choices=DEVICE_TYPE_CHOICES, default='Smartphone')
    devBrand = models.CharField(max_length=20)
    devModel = models.CharField(max_length=100, default="")
    
    #password y tipo de password del dispositivo
    passPin = models.CharField(max_length=10, blank=True, null=True)
    passWord = models.CharField(max_length=100, blank=True, null=True)
    passPatt = models.CharField(max_length=50, blank=True, null=True)

    #condiciones del equipo al recibirlo
    con_devPower=models.CharField(max_length=10, default='OFF', choices=DEV_POWER_CHOICES, blank=False)
    con_devAssembly=models.CharField(max_length=10, default='ARM', choices=DEV_ASSEMBLY_CHOICES, blank=False)
    con_devChargeCable=models.CharField(max_length=10, default='CON', choices=DEV_CHARGECABLE_CHOICES, blank=False)
    con_devChargeAdapter=models.CharField(max_length=10, default='CON', choices=DEV_CHARGEADAPTER_CHOICES, blank=False)
    con_devBattery=models.CharField(max_length=10, default='CON', choices=DEV_BATTERY_CHOICES, blank=False)
    con_devSIM=models.CharField(max_length=10, default='CON', choices=DEV_SIM_CHOICES, blank=False)
    con_devCase=models.CharField(max_length=10, default='CON', choices=DEV_CASE_CHOICES, blank=False)
    con_devSDCard=models.CharField(max_length=10, default='CON', choices=DEV_SDCARD_CHOICES, blank=False)

# Create your models here.
class Repair(models.Model):

    #make instance id user id instead
    def get_image_path(self, instance, filename):
        return os.path.join('/media/', instance.idCase, filename)

    STATUSES=(
        ('NA', 'N/A'),
        ('COT', 'Cotizacion'),
        ('PREP', 'Reparacion'),
        ('REP', 'Reparado. Listo para Entrega'),
        ('ENT', 'Entregado/Fuera de Tienda'),
        ('DES', 'Reparacion/Cotizacion no Realizada'),
    )

    idCase = models.UUIDField(default=uuid.uuid4, editable=False, blank=False)
    ##slug = models.SlugField()
    device = models.ForeignKey(Device, default=None, on_delete=models.CASCADE)
    startDate = models.DateTimeField(auto_now_add=True)
    lastModified = models.DateTimeField(auto_now=True)
    probDescription = models.TextField()
    technician = models.ForeignKey('accounts.Usuario', on_delete=models.CASCADE, null=True, blank=True)
    repairDetail = models.TextField(blank=True)
    repairCost = models.DecimalField(max_digits=8, decimal_places=2, validators=[MinValueValidator(0.00)], blank=True, default=0.00)
    payment = models.OneToOneField('repairtracker.Payment', default=None, on_delete=models.CASCADE)
    


    def __str__(self):
        return str(self.idCase)


    def snippet(self):
        return str(self.probDescription[:50])

    @classmethod
    def create(cls, idCase):
        repair = cls(idCase=idCase)
        return repair



class Cellphone(models.Model):
    device = models.ForeignKey(Device, on_delete=models.CASCADE, blank=True)
    imei = models.CharField(max_length=30, null=True, blank=True)
    cel_number = models.CharField(max_length=30, null=True, blank=True)
    provider = models.CharField(max_length=30, null=True, blank=True)
    

class Images(models.Model):
    repair = models.ForeignKey(Repair, default=None, on_delete=models.CASCADE)
    image = models.ImageField(blank=True, null=True, verbose_name='Image')

'''
class Complaint(models.Model):
    repair = models.ForeignKey(Repair, default=None, on_delete=models.CASCADE)
    description = models.CharField(max_length=300, null=True)
'''


class Repair_status(models.Model):
    STATUSES=(
        ('NA', 'N/A'),
        ('COT', 'Cotizacion'),
        ('PREP', 'Reparacion'),
        ('REP', 'Reparado. Listo para Entrega'),
        ('ENT', 'Entregado/Fuera de Tienda'),
        ('DES', 'Reparacion/Cotizacion no Realizada'),
    )
    repair = models.ForeignKey(Repair, default=None, on_delete=models.CASCADE)
    date_time = models.DateTimeField(auto_now_add=True)
    repairStatus = models.CharField(max_length=5, choices=STATUSES, default = 'PREP')
    statusDetail = models.CharField(max_length = 100, null=True)
    


class Payment(models.Model):
    P_STATUSES =(
        ('PEN', 'Pendiente'),
        ('DONE', 'Pagado'),
        ('DISC', 'Descartado')
    )

    P_TYPES = (
        ('CARD', 'Tarjeta'),
        ('CASH', 'Efectivo')
    )

    payment_id = models.AutoField(primary_key=True)
    date_created = models.DateTimeField(auto_now_add=True)
    date_closed = models.DateTimeField()
    payment_type = models.CharField(max_length=5, choices = P_TYPES, default='CASH')
    payment_status = models.CharField(max_length=5, choices= P_STATUSES, default='PEN')
    total_cost = models.DecimalField(max_digits=15, decimal_places=2, null=True)
    received = models.DecimalField(max_digits=15, decimal_places=2, null=True)
    change = models.DecimalField(max_digits=15, decimal_places=2, null=True)

class Concept(models.Model):
    payment = models.ForeignKey(Payment, default=None, on_delete=models.CASCADE)
    name = models.CharField(max_length = 30, null=True)
    cost = models.DecimalField(max_digits=15, decimal_places=2, null=True)
    quantity = models.SmallIntegerField(default=1)


    


    
