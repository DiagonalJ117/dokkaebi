import os
import base64
import socket
from uuid import UUID
from datetime import datetime
from django.shortcuts import render, get_object_or_404, redirect
from .models import Repair, Images
from django.http import HttpResponse, JsonResponse
from django.conf import settings
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.admin.views.decorators import staff_member_required
from django.views import generic
from . import forms
from django.forms import modelformset_factory
from django.core.exceptions import ValidationError
from django.urls import reverse
from accounts.models import Usuario, Client, Employee
from django.contrib.auth.models import AnonymousUser
from django.contrib import messages
from django.db.models import Q
from dokkaebirepair.decorators import permissions_required

from django.views.generic import View

from django.template.loader import get_template


#from . import urls

# Create your views here.

#filter repairs by username if user is not staff and if its authenticated
#if user is not signed in, repair tracker appears but does not show anything UNLESS USER is staff
#if user inputs an id_case, (no need to be logged in) it'll lead him to the repairdetail
#@permission_required('accounts.is_collab', raise_exception=True)
@login_required(login_url="accounts:login")
@permissions_required('can_view_repairs')
def repairdashboard(request):
    """
    completed = Repair.objects.all().filter(repairStatus = 'ENT')
    pending_invoices = Repair.objects.all().filter(repairStatus = 'COT')
    pending_repairs = Repair.objects.all().filter(repairStatus = 'PREP')
    pending_ir = Repair.objects.all().filter(Q(repairStatus = 'PREP') | Q(repairStatus = 'COT'))
    pending_delivery = Repair.objects.all().filter(repairStatus = 'REP')
    discarded = Repair.objects.all().filter(repairStatus = 'REP')
    month_repairs = Repair.objects.all().filter(startDate__month = datetime.now().month)
    """
    return render(request, 'repairtracker/dashboard.html')#, {'completed': completed, 'pending_delivery': pending_delivery, 'pending_ir': pending_ir, 'month_repairs': month_repairs, 'discarded': discarded, 'pending_invoices': pending_invoices, 'pending_repairs': pending_repairs})

@login_required(login_url="/accounts/login/")
@permissions_required('can_view_repairs', 'can_view_repair')
def repairtracker(request):
    user = request.user
    if user.is_authenticated:
        repairs=Repair.objects.all().order_by('-startDate')
        return render(request, 'repairtracker/repairtracker.html', {'repairs': repairs})
    else:
        return render(request, 'repairtracker/repairsearch.html')
        #repairs=Repair.objects.all().filter(user.username)
        #return render(request, 'repairtracker/repairtracker.html', {'repairs': repairs})


#@permission_required('accounts.is_collab', raise_exception=True)
@login_required(login_url="/accounts/login/")
def repairdetail(request, idCase):
    user = request.user
    #return HttpResponse(idCase)
    repair = Repair.objects.get(idCase=idCase)
    repair = get_object_or_404(Repair, idCase=idCase)

    images = Images.objects.all().filter(repair=repair)
    if request.method == 'POST':
        form = forms.UpdateStatus(request.POST or None, instance=repair)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.save()
            messages.success(request, 'Status Actualizado')
            return redirect('repairtracker:repair-detail', repair.idCase)
        else:
            messages.warning(request, 'Error al Actualizar')
            return redirect('repairtracker:repair-detail', repair.idCase)
    else:
        form = forms.UpdateStatus()
    #status = Status.objects.get(repair=repair)
    return render(request, 'repairtracker/repairdetail.html', 
    {
    'repair': repair, 
    'images': images, 
    'form':form
    })

@login_required(login_url="/accounts/login/")
@permission_required('can_change_repair')
def repairupdate(request, idCase):
    #return HttpResponse(idCase)
    user = request.user
    ImageFormSet = modelformset_factory(Images, form=forms.ImageForm, extra=5)
    
    repair = Repair.objects.get(idCase=idCase)
    repair = get_object_or_404(Repair, idCase=idCase)
    if request.method == 'POST':
        form = forms.UpdateRepair(request.POST or None, instance=repair)
        formset = ImageFormSet(request.POST, request.FILES, queryset=Images.objects.none())
        if form.is_valid() and formset.is_valid():
            instance = form.save(commit=False)
            instance.save()
            for form in formset:
                data = form.cleaned_data
                image = data.get('image')
                #image = form['image']
                photo = Images(repair=instance, image=image)
                if(photo.image):
                    photo.save()

            
            return redirect('repairtracker:repair-detail', repair.idCase)

    else:
        form = forms.UpdateRepair(instance=repair)
        formset = ImageFormSet(queryset=Images.objects.none())

    #status = Status.objects.get(repair=repair)


    return render(request, 'repairtracker/repairupdate.html', 
    {'repair': repair, 
    'form': form, 
    'formset': formset})

@login_required(login_url="/accounts/login/")
@permission_required('can_delete_repair')
def repairdelete(request, idCase):
    repair = Repair.objects.get(idCase=idCase)
    repair.delete()
    messages.info(request,'Caso Borrado!')
    return redirect('repairtracker:repair-list')

@login_required(login_url="/accounts/login/")
@permission_required('can_delete_image')
def delete_repair_image(request, idCase, idPic):
    repair = Repair.objects.get(idCase=idCase)
    image = Images.objects.get(id=idPic)
    image.delete()
    messages.info(request,'Imagen borrada')
    return redirect('repairtracker:repair-detail', repair.idCase)

@login_required(login_url="/accounts/login/")
@permission_required('can_add_repair')
#@permission_required('accounts.is_collab', raise_exception=True)
def repaircreate(request):
    clients = Client.objects.all()

    user = request.user

    ImageFormSet = modelformset_factory(Images, form=forms.ImageForm, extra=5)


    if request.method == 'POST':

        device_form = forms.CreateDevice(request.POST)
        repair_form = forms.CreateRepair(request.POST, request.FILES)
        cel_form = forms.CreateCellphone(request.POST)
        formset = ImageFormSet(request.POST, request.FILES, queryset=Images.objects.none())


        if device_form.is_valid() and repair_form.is_valid() and formset.is_valid():
            #save repair to database
            #in = instance
            repair_in= repair_form.save(commit=False)
            device_in = device_form.save(commit=False)
            cel_in =  cel_form.save(commit=False)
                

            if user.role == 2:
                device_in.technician = request.user
            repair_in.createdBy = request.user
            #instance.userEmail = request.user.email
            device_in.save()
            repair_in.save()
            if device_in.devType == 'PHONE':
                cel_in.save()
            for form in formset:
                data = form.cleaned_data
                image = data.get('image')
                #image = form['image']
                photo = Images(repair=repair_in, image=image)
                if(photo.image):
                    photo.save()
            return redirect('repairtracker:repair-list')

    else:
        device_form = forms.CreateDevice()
        repair_form = forms.CreateRepair()
        cel_form = forms.CreateCellphone()
        formset = ImageFormSet(queryset=Images.objects.none())


    return render(request, 'repairtracker/repaircreate.html', 
    {
    'device_form': device_form, 
    'repair_form': repair_form, 
    'cel_form':cel_form, 
    'clients': clients, 
    'formset': formset
    })

@login_required(login_url="/accounts/login/")
@permission_required('can_view_repair')
def repairsearch(request):
    return render(request, 'repairtracker/repairsearch.html')
    
#@login_required(login_url="/accounts/login/")
def search(request):
    if 'q' in request.GET:
        try:
            uuid_string=request.GET.get('q')
            uuid_test = UUID(uuid_string, version=4)
        except ValueError:
            messages.error(request, "Número de rastreo incorrecto o no existente")
            return redirect('repairtracker:repair-search')
        return redirect(reverse('repairtracker:repair-list') + request.GET.get('q'))
    else:
        return redirect(request, 'repairtracker:repair-search')

    return render(request, 'repairtracker/repairsearch.html')



def image_as_base64(image_file, format='png'):
    """
    :param `image_file` for the complete path of image.
    :param `format` is format for image, eg: `png` or `jpg`.
    """
    if not os.path.isfile(image_file):
        return None
    
    encoded_string = ''
    with open(image_file, 'rb') as img_f:
        encoded_string = base64.b64encode(img_f.read())
    return 'data:image/%s;base64,%s' % (format, encoded_string)


def ticket_ingreso(request, idCase):
    repair = Repair.objects.get(idCase=idCase)
    context = {
            "repair": repair,
        }
 
    return render(request, 'repairtracker/ticket_ingreso.html', context)

"""def print_pdf(request, idCase):
    repair = Repair.objects.get(idCase=idCase)
    hostname = socket.gethostname()
    ip_addr = socket.gethostbyname(hostname)
    #ip_addr= 'localhost'
    #ip_addr = '0.0.0.0'
    context = {
            "repair": repair,
        }
    root = ip_addr+":8000"
    options = {
        'orientation': "Landscape",
        'page-size': "A3",
        'margin-right': "0in",
        'margin-left': "0in",
        'margin-top': "0.5in",
        'margin-bottom': "0in",
        'encoding': "UTF-8",
        'dpi': "300",
        'javascript-delay': "0",
        'background': None
    }

    
    filename = "ticket_orden_"+str(repair.id)+".pdf"
    response = HttpResponse(pdf, content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="' + filename + '"'
    return response


    #return render(request, 'repairtracker/ticket_ingreso.html', context)

#def update_status(request, idCase):
#   repair = Repair.objects.get(idCase=idCase)
#    if request.method == 'POST':
#        form = forms.UpdateStatus(request.POST)
#        if form.is_valid():
#            form.save()
#            messages.success(request, 'Status Actualizado')
#            return redirect('repairtracker:repair-detail', repair.idCase)
#        else:
#            messages.warning(request, 'Error al Actualizar')
#            return redirect('repairtracker:repair-detail', repair.idCase)
#    else:
#        form = forms.UpdateStatus()
#    return redirect('repairtracker:repair-detail', repair.idCase)

def print_ticket_sale(request, idCase):
    repair = Repair.objects.get(idCase=idCase)
    template = get_template('ticket_venta.html')
    context = {
            "repair": repair,
        }
    #pdf = render_to_pdf('ticket_venta.html', context)
    return HttpResponse('Ticket de Venta')
    #return HttpResponse(pdf, content_type='application/pdf')
"""
def __str__(self):
    return self.get_devType_display()

