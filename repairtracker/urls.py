from django.urls import path
from repairtracker import views
#from .views import print_ticket

app_name = 'repairtracker'

urlpatterns = [
    path('', views.repairdashboard, name="repair-dashboard"),
    path('repairlist', views.repairtracker, name="repair-list"),
    path('create', views.repaircreate, name="repair-create"),
    path('<str:idCase>/ticket', views.ticket_ingreso, name="view-ticket"),
    path('<str:idCase>/<str:idPic>/delete', views.delete_repair_image, name="delete-repair-image"),
    path('<str:idCase>/delete', views.repairdelete, name="repair-delete"),

    path('<str:idCase>', views.repairdetail, name="repair-detail"),
    #path('<str:idCase>/statusUpdate', views.update_status, name="status-update"),
    path('<str:idCase>/update', views.repairupdate, name="repair-update")
    
    #path('<str:idCase>', views.search, name="search"),

]
