from django import forms
from repairtracker.models import Repair, Images, Device, Repair_status, Cellphone

class CreateRepair(forms.ModelForm):
    required_css_class = 'required'

    class Meta:
        model = Repair
        fields = ['technician', 'device' , 'probDescription', 'repairCost']
        labels = {
            'technician': 'Tecnico',
            'device': 'Dispositivo',
            'probDescription': 'Problema',
            'repairCost': 'Costo total de Reparacion',
        }

        widgets = {
            'probDescription': forms.Textarea(attrs={'onkeyup':'auto_grow(this)', 'rows':'2'}),
            'repairCost': forms.TextInput(attrs={'placeholder': '0.00'}),
        }

class CreateDevice(forms.ModelForm):
    required_css_class = 'required'
    def clean(self):
        serialNumber = self.cleaned_data.get('serialNumber')
        imeiNumber = self.cleaned_data.get('imeiNumber')

        if not serialNumber and not imeiNumber:
            msg = forms.ValidationError("No. Serial o IMEI requerido. No dejar ambos en blanco!")
            self.add_error('serialNumber', msg)
            self.add_error('imeiNumber', msg)
            raise msg
    
    class Meta:

        DEV_POWER_CHOICES=(
            ('ON', 'Encendido'),
            ('OFF', 'Apagado') 
        )


        DEV_ASSEMBLY_CHOICES=(
            ('ARM', 'Armado'),
            ('DES', 'Desarmado')
        )


        DEV_CHARGECABLE_CHOICES=(
            ('CON', 'Con Cable de Carga'),
            ('SIN', 'Sin Cable de Carga'),
        )

        DEV_CHARGEADAPTER_CHOICES=(
            ('CON', 'Con Adaptador de Carga'),
            ('SIN', 'Sin Adaptador de Carga'),
        )


        DEV_BATTERY_CHOICES=(
            ('CON', 'Con Bateria'),
            ('SIN', 'Sin Bateria')
        )

        DEV_SIM_CHOICES=(
            ('CON', 'Con Tarjeta SIM'),
            ('SIN', 'Sin Tarjeta SIM')
        )

        DEV_CASE_CHOICES=(
            ('CON', 'Con Funda'),
            ('SIN', 'Sin Funda')
        )

        DEV_SDCARD_CHOICES=(
            ('CON', 'Con Memoria'),
            ('SIN', 'Sin Memoria')
        )
        model=Device
        fields = [
            'serialNumber',
            'client',
            'devType',
            'devBrand',
            'devModel',
            'passPin',
            'passWord',
            'passPatt',
            'con_devPower',
            'con_devAssembly',
            'con_devChargeCable',
            'con_devChargeAdapter',
            'con_devSIM',
            'con_devCase',
            'con_devSDCard',
            'con_devBattery',
        ]
        labels = {
            'serialNumber': 'No. Serial',
            'client':'Cliente',
            'devType': 'Tipo',
            'devBrand': 'Marca',
            'devModel': 'Modelo',
            'passPin': 'PIN',
            'passWord': 'Password',
            'passPatt': 'Patron',
            'con_devPower': '',
            'con_devAssembly': '',
            'con_devChargeCable': '',
            'con_devChargeAdapter': '',
            'con_devSIM': '',
            'con_devCase': '',
            'con_devSDCard': '',
            'con_devBattery': '',
        }
        widgets = {
            'con_devPower': forms.RadioSelect(attrs={'onClick':'devConditionCheck_power();'},choices=DEV_POWER_CHOICES),
            'con_devAssembly': forms.RadioSelect(attrs={'onClick':'devConditionCheck_assembly();'},choices=DEV_ASSEMBLY_CHOICES),
            'con_devChargeCable': forms.RadioSelect(attrs={},choices=DEV_CHARGECABLE_CHOICES),
            'con_devChargeAdapter': forms.RadioSelect(attrs={},choices=DEV_CHARGEADAPTER_CHOICES),
            'con_devSIM': forms.RadioSelect(attrs={},choices=DEV_SIM_CHOICES),
            'con_devCase': forms.RadioSelect(attrs={},choices=DEV_CASE_CHOICES),
            'con_devSDCard': forms.RadioSelect(attrs={},choices=DEV_SDCARD_CHOICES),
            'con_devBattery': forms.RadioSelect(attrs={'onClick':'devConditionCheck_battery();'},choices=DEV_BATTERY_CHOICES),
            'passPin': forms.PasswordInput(),
            'passWord': forms.PasswordInput(),
        }
        
class CreateCellphone(forms.ModelForm):
    class Meta:
        model = Cellphone
        fields = ['cel_number', 'imei', 'provider']
        labels = {
            'cel_number' : 'Numero Celular',
            'imei' : 'Numero IMEI',
            'provider': 'Compañia Telefonica'
        }


class UpdateRepair(forms.ModelForm):

    
    class Meta:
        model = Repair
        fields = ['technician', 'device' , 'probDescription', 'repairCost']
        labels = {
            'technician': 'Tecnico',
            'device': 'Dispositivo',
            'probDescription': 'Problema',
            'repairCost': 'Costo total de Reparacion',
        }

        widgets = {
            'probDescription': forms.Textarea(attrs={'onkeyup':'auto_grow(this)', 'rows':'2'}),
            'repairCost': forms.TextInput(attrs={'placeholder': '0.00'}),
        }


class ImageForm(forms.ModelForm):
    image = forms.ImageField(label='Imagen')
    class Meta:
        model = Images
        fields = ('image', )

class CreateStatus(forms.ModelForm):
    class Meta:
        model=Repair_status
        fields = ['repair','repairStatus']
        labels = {
            'repairStatus': 'Estado de Reparacion',
            'repair': 'Reparacion',
            
        }


class UpdateStatus(forms.ModelForm):
    class Meta:
        model = Repair_status
        fields = ['repair','repairStatus']
        labels = {
            'repairStatus': 'Estado de Reparacion',
            'repair': 'Reparacion',
            
        }

