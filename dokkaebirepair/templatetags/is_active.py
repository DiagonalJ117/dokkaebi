import re

from django import template
from django.urls import reverse
register = template.Library()

@register.simple_tag(takes_context=True)
def is_active(context, url):
    try:
        pattern = '^%s$' % reverse(url)
    except NoReverseMatch:
        pattern = url

    path = context['request'].path
    return "active" if re.search(pattern,path) else ''

#    if request.path in reverse(url):
#        return "active"
#    return ""
