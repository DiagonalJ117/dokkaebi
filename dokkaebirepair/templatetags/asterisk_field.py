from django import template
from django.urls import reverse
register = template.Library()

@register.filter
def append_ast_if_req (field):
    if field.field.required and field.label != '':
         return field.label + '*'
    else:
         return field.label