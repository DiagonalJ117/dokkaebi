from django.contrib.auth import REDIRECT_FIELD_NAME
from django.contrib.auth.decorators import user_passes_test

def employee_required(function=None, redirect_field_name = REDIRECT_FIELD_NAME, login_url = 'login'):
    actual_decorator = user_passes_test(
        lambda u: u.is_active and u.role == 'EM',
        login_url=login_url,
        redirect_field_name=redirect_field_name
    )

    if function:
        return actual_decorator(function)
    return actual_decorator

def client_required(function=None, redirect_field_name = REDIRECT_FIELD_NAME, login_url = 'login'):
    actual_decorator = user_passes_test(
        lambda u: u.is_active and u.role == 'CL',
        login_url=login_url,
        redirect_field_name=redirect_field_name
    )

    if function:
        return actual_decorator(function)
    return actual_decorator

def roles_required(*roles):
    return user_passes_test(lambda u: any(u.role == role for role in roles))

def permissions_required(*perms):
    return user_passes_test(lambda u: any(u.has_perm(perm) for perm in perms))